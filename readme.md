# Desafio 1
Desafio do curso de Docker do [Curso Full Cycle](fullcycle.com.br/)  
Link da imagem: [https://hub.docker.com/r/atauapd/desafio1-final](https://hub.docker.com/r/atauapd/desafio1-final)

```bash
docker run atauapd/desafio1-final
```